'''
docparser.py
Author: Alex Case
Email: alexwcase@gmail.com
Takes urls from urls.manifest and
 parses them into a document
 '''


import urllib.request
from bs4 import BeautifulSoup
import re
from html.parser import HTMLParser

'''
Extends HTMLParse to remove html tags 
and compile the remaining content 
'''
class MLStripper(HTMLParser):
	def __init__(self):
		super(MLStripper, self).__init__()
		self.stripped=[]
	#def handle_starttag(self, tag, attrs):
		#print("Encountered a start tag:", tag)
	#def handle_endtag(self, tag):
		#print("Encountered an end tag :", tag)
	def handle_data(self, data):
		#print("Encountered some data  :", data)
		self.stripped.append(' ' + data)
	def get_data(self):
		return self.stripped

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

'''
Remove some special characters
TODO: Set up a regex to find and remove these
'''
def strip_punctuation(html):
	html = html.replace('?', ' ')
	html = html.replace('!', ' ')
	html = html.replace('-', ' ')
	html = html.replace(',', ' ')
	html = html.replace('.', ' ')
	html = html.replace('(', ' ')
	html = html.replace(')', ' ')
	html = html.replace('\"', ' ')
	html = html.replace('\'', ' ')
	html = html.replace('[', ' ')
	html = html.replace(']', ' ')
	html = html.replace('[\'', ' ')
	html = html.replace(']\'', ' ')
	html = html.replace('\\n', ' ')
	html = html.replace('\\b', ' ')
	html = html.replace('\\t', ' ')
	html = html.replace('\\', '')
	html = html.replace('\' ', ' ' )
	html = html.replace(':', ' ')
	html = html.replace(';', ' ')
	html = html.replace('|', ' ')
	html = html.replace(' b ', '') #TODO Actually fix this
	return html

def strip_headers(html):
	html = html.replace('Welcome! These links will help you explore Bridgewater and find detailed information quickly and easily.', '')
	html = html.replace('As parents of prospective or current students, you can follow these links to learn more about the university and our parent resources and services.', '')
	html = html.replace('The alumni sections of our site provide you with news, resources, and information about alumni services and how to stay involved.', '')
	return html
'''
Optional function for matching a regex
'''
def match(string):
	ma = re.search('<([A-Z][A-Z0-9]*)\b[^>]*>(.*?)</\1>', string)
	if ma is None:
		return False	
	else:
		return True

'''
Creates a file name from a url
Replaces all / with #
'''
def create_file_name(url):
	url = url.replace('http://', '')
	url = url.replace('/', '#')
	return url

#Read in the list of urls to parse
urllist = [line.strip() for line in open('urls.manifest')]



for url in urllist:
	if "www.bridgew.edu" in url:
		filename = create_file_name(url)
		# try:
		# 	urllib.request.urlretrieve(url, filename)
		# except Exception as err:
		# 	print("--FAILED", err)
		try:
			html = urllib.request.urlopen(url).read() #read the url
		except Exception as err:
			print("--FAILED ", url, err) 
		htmlsoup = BeautifulSoup(html) #Convert the html into BeautifulSoup for parsing
		meat = htmlsoup.findAll('p')
		meat = strip_headers(str(meat))
		title = htmlsoup.findAll('title')
		print(title)
		if(title):
			meat = str(title[0]) + meat



		meat = str(meat).encode('ascii', 'ignore')
		meat = strip_tags(str(meat))
		meat = strip_punctuation(str(meat))


		with open('documents/raw/'+filename,'w') as f:
			f.write(str(meat))
