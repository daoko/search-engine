'''
crawler.py
Author: Alex Case 
Email: alexwcase@gmail.com
Web Crawler 
Takes a seed URL and performs breadth-first crawl of html pages
Saves urls in urls.manifest
'''
import sys

import urllib.parse
import urllib.request
from bs4 import BeautifulSoup 
import shutil

#reconstruct urls which don't include the full path
def frankenstein(root_url, path):
	domain = root_url
	#print (root_url, "and", path)
	path = urllib.parse.urljoin(domain, path)
	return path


#The seed URL
if len(sys.argv) > 1:
	url = sys.argv[1]
else:
	url = "http://www.bridgew.edu"

#A Queue for the scraped URLS
urls = [url]
#And a queue to keep track of what has been seen already
url_history = [url]

#A loop to read HTML files while the queue has something to read
times=0 #optional counter for limiting the loop
while len(urls) > 0 and len(url_history) < 10000:
	print("======== PARSING ", urls[0], " ========")
	try:
		html = urllib.request.urlopen(urls[0]).read() #read the url
	except Exception as err:
		print("--FAILED ", urls[0], err) 
	htmlsoup = BeautifulSoup(html) #Convert the html into BeautifulSoup for parsing

	

	#Parse links with BeautifulSoup
	#find all of the anchor tags that have an href
	for tag in htmlsoup.findAll('a', href=True):
		#Use urllib.parse.urljoin(2) to reconstruct relative url paths
		try:
			link = frankenstein(urls[0], tag['href'])	
		except Exception as inst:
			print("oops", inst)
		#print(tag['href'])
		#only keep urls that belong to this domain
		if url in tag ['href'] and tag['href'] not in url_history:
			url_history.append(tag['href'])
			urls.append(tag['href'])
			#download the html
			#try:
			#	file_name = str(times)
			#	urllib.request.urlretrieve(tag['href'], file_name)
			#except Exception as err:
			#	print("--FAILED download of ", tag['href'], err)
	urls.pop(0) #pop the item we just downloaded
	times+=1
	print("======== LOGGED ", len(url_history), " URLS  ========")
	print("======== VISITED ", times, " URLS ========")

with open("urls.manifest", 'a') as out:
	for link in url_history:
		out.write(link + '\n')



	



