'''
db.py
Author: Alex Case
Email: alexwcase@gmail.com
Used for setting up and operating on the database
'''

import sqlite3
import stemquery
import sys
import math


conn = sqlite3.connect('C:\Home\Web IR\Search Engine\db\index.db')
c = conn.cursor()

'''
3 Tables will be used
Words: ID INT, keyword TEXT
	ID: the unique identifier of the Word
	keyword: the word 


Documents: ID INT, url TEXT, raw TEXT, document TEXT
	ID: the unique identifier of the document 
	url: the source of the document 
	raw: the content extracted from the url
	document: the stemmed content of the url

InvertedIndex: ID INT, WID INT, DOCID INT
	ID: unique identifier for the entry
	WID: the ID of the corresponding Word 
	DOCID: The ID of the document that contains this word 
	word: the word the index represents 
	doc: the document the word is in

'''

def createTables():
	c.execute("CREATE TABLE words(id INT UNIQUE, keyword TEXT UNIQUE) ")
	c.execute("CREATE TABLE documents(id INT UNIQUE, url TEXT UNIQUE, raw TEXT, document TEXT)")
	c.execute("CREATE TABLE invertedIndex(id INT UNIQUE, WID INT, DOCID INT, word TEXT, doc TEXT)")

'''
Write operations
'''
def insertWord(kw):
	c.execute("SELECT max(id) FROM words")
	id = c.fetchone()[0]
	if id == None:
		id = 0
	id += 1
	c.execute("INSERT INTO words (id, keyword) VALUES (?,?)",
		(id, kw))
	conn.commit()
	return id

def insertDoc(url, raw, doc):
	c.execute("SELECT max(id) FROM documents")
	id = c.fetchone()[0]
	if id == None:
		id = 0
	id += 1
	c.execute("INSERT INTO documents (id, url, raw, document) VALUES (?,?,?,?)",
		(id, url, raw, doc))
	conn.commit()
	return id

def insertInvIndex(word, did, doc):
	c.execute("SELECT max(id) FROM invertedIndex")
	id = c.fetchone()[0]
	if id == None:
		id = 0
	id += 1

	c.execute("SELECT * FROM words WHERE keyword = ?", (word,))
	wid = c.fetchone()[0]
	#print(wid)

	c.execute("INSERT INTO invertedIndex (id, WID, DOCID, word, doc) VALUES (?,?,?,?,?)",
		(id, wid, did, word, doc))
	conn.commit()
	return id

'''
TODO:
Read operations
'''

def getwordID(word):
        try:
                c.execute("SELECT * FROM words WHERE keyword = ?", (word,))
                wid = c.fetchone()[0]
                return wid
        except Exception as err:
                print("could not find ", word)
                return -1

def getQueryIDs(query):
        query = stemquery.stemQuery(query)
        queryWIDs = []
        for word in query:
                wid = getwordID(word)
                queryWIDs.append(wid)
        return queryWIDs

def generateDocs(query):
        wids = getQueryIDs(query)
        docIDs = []
        for wid in wids:
                c.execute("SELECT * FROM invertedIndex WHERE WID = ?", (wid,))
                rows = c.fetchall()
                for row in rows:
                        if ([row[2], 0.0]) not in docIDs:
                                docIDs.append([row[2],0.0])
        return docIDs
        

def vsmnum(did, query):
        queryWIDs = getQueryIDs(query)
        result = 0
        for  wid in queryWIDs:
                c.execute("SELECT * FROM invertedIndex WHERE WID = ? AND DOCID = ?", (wid, did))                        
                result += len(c.fetchall())

        #factor in urls
        c.execute("SELECT url FROM documents WHERE ID = ?", (did,))
        url = c.fetchone()[0].split('/')
        for i in range(4, len(url)):
                for word in query.split():
                        print(word, url[i])
                        if word == url[i]:
                                result+= 1
                
        return result

def vsmdenom(did, query):
        queryWIDs = getQueryIDs(query)
        document_magnitude = 0
        query_magnitude = 0

        '''
        Calculate document magnitude
        '''
        seen = []
        c.execute("SELECT * FROM invertedIndex WHERE DOCID = ?", (did,))
        allwords = c.fetchall()

        for word in allwords:
                if word[1] not in seen:
                        seen.append(word[1])
                        
        doc_vector = []
        for word in seen:
                c.execute("SELECT * FROM invertedIndex WHERE DOCID = ? AND wid = ?", (did, word))
                doc_vector.append(len(c.fetchall()))

        for i in doc_vector:
                document_magnitude += i**2


        '''
        Calculate query magnitude
        '''
        query_vector = []
        seen = []
        for word in query.split():
                if word not in seen:
                        seen.append(word)
                        
        for word in seen:
                query_vector.append(query.count(word))
                

        for i in query_vector:
                query_magnitude += i**2
                
        return (document_magnitude **(0.5)) * (query_magnitude **(0.5))


def TF_IDFnum(did, query):
        queryWIDs = getQueryIDs(query)
        result = 0
        for  wid in queryWIDs:
                c.execute("SELECT * FROM invertedIndex WHERE WID = ? AND DOCID = ?", (wid, did))                        
                result += len(c.fetchall())

        #factor in urls
        c.execute("SELECT url FROM documents WHERE ID = ?", (did,))
        url = c.fetchone()[0].split('/')
        for i in range(4, len(url)):
                for word in query.split():
                        print(word, url[i])
                        if word == url[i]:
                                result+= 1
        
        return result / maxV(did)

def TF_IDFdenom(did, query):
        queryWIDs = getQueryIDs(query)
        document_magnitude = 0
        query_magnitude = 0
        weight1 = maxV(did)

        '''
        Calculate document magnitude
        '''
        seen = []
        c.execute("SELECT * FROM invertedIndex WHERE DOCID = ?", (did,))
        allwords = c.fetchall()

        for word in allwords:
                if word[1] not in seen:
                        seen.append(word[1])
                        
        doc_vector = []
        for word in seen:
                c.execute("SELECT * FROM invertedIndex WHERE DOCID = ? AND wid = ?", (did, word))
                doc_vector.append((len(c.fetchall()) / weight1) * IDF(word))

        for i in doc_vector:
                document_magnitude += i**2


        '''
        Calculate query magnitude
        '''
        query_vector = []
        seen = []
        for word in query.split():
                if word not in seen:
                        seen.append(word)
                        
        for word in seen:
                query_vector.append(query.count(word))
                

        for i in query_vector:
                query_magnitude += i**2
                
        return (document_magnitude **(0.5)) * (query_magnitude **(0.5))

def IDF(wid):
        c.execute("SELECT * FROM invertedIndex WHERE wid = ?", (wid,))
        n = len(c.fetchall())
        return math.log(N / n)

def maxV(did):
        c.execute("SELECT WID FROM InvertedIndex WHERE DOCID = ?", (did,))
        doc = c.fetchall()
        top = max(set(doc), key=doc.count)
        return doc.count(top)
        
        

        



def TF_IDF(did, query):
        return TF_IDFnum(did, query) / TF_IDFdenom(did, query)
        #return vsmnum(did, query) / vsmdenom(did, query)


        

def handleQuery(query):
        docs = generateDocs(query)
        for doc in docs:
                doc[1] = TF_IDF(doc[0], query)

        docs.sort(key=lambda x: float(x[1]))
        docs.reverse()

        for doc in docs:
               print(doc)

        with open('results.q', 'w') as f:
                for doc in docs:
                        c.execute("SELECT url FROM documents WHERE ID = ?", (doc[0],))
                        url = c.fetchone()[0] + "\n"
                        f.write(url)
        

if __name__ == "__main__":
        c.execute("SELECT * FROM documents")
        N = len(c.fetchall())
        if len(sys.argv) > 1:
            print (sys.argv[1])
            handleQuery(sys.argv[1])
        else:
            try:
                    createTables()
            except Exception as err:
                    print("database already created")




