'''
invertedindexer.py 
Author: Alex Case
Email: alexwcase@gmail.com
Creates an inverted index of the words and documents
'''

import db
from os import walk

'''
Words in the stoplist will not be added to the db
'''
stoplist = ['a', 'the', 'of', 'bridgewat', 'and', 'it', 'an', 'b', '[\'', ']', '\']', 'this',
			 'that', 'are', 'was', 'can', 'or', 'to', 'our', 'with', 'how', 'by', 'on', "is", "in",
			 "+" ]

def converturl(url):
	url = url.replace('#','/')
	return 'http://' + url

def populateDB():
	rawfiles = []
	for (dirpath, dirnames, filenames) in walk("..\documents\\raw"):
		rawfiles.extend(filenames)
		break
	#print(rawfiles)

	stemmedfiles = []
	for (dirpath, dirnames, filenames) in walk("..\\documents\\stemmed"):
		stemmedfiles.extend(filenames)
		break
	#print(stemmedfiles)

	for i in range(0, len(stemmedfiles)-1):
                
		sfile = open("..\\documents\\stemmed\\"+stemmedfiles[i-1])
		rfile = open("..\\documents\\raw\\"+rawfiles[i-1])
		url = converturl(stemmedfiles[i-1])
		print(url)
		stemdoc = sfile.readline()
		rawdoc = sfile.readline()

		'''
		Create the document
		'''
		try:
			docid = db.insertDoc(url, rawdoc, stemdoc)
		except Exception as err:
			print("Document Entry failed, probably duplicate url:\n", err)

		'''
		Enter each word in the doc into the words Table
		'''
		for word in stemdoc.split():
			if word not in stoplist:
				try:
					wid = db.insertWord(word)
				except Exception as err:
					print("Word Entry failed, probably duplicate entry:\n", err)
				try:
					db.insertInvIndex(word, docid, url)
				except Exception as err:
					print("Entry failed\n", err)




if __name__ == "__main__":
	populateDB()
